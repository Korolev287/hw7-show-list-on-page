// Браузер просматривает весь html и создает дерево елементов, 
// все начинается с елемента document, потом в него вкладывается елемент html, 
// куда вкладывается head и body. 
// Дальше он вкладывает каждый елемент друг в друга в правильной последовательности.
// DOM позволяет нам получать доступ к елементам страницы с помощью javascript, 
// за счет этого мы можем работать с каждым елементом на странице читая его свойста и вызывая его методы.


const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const domPosition = document.getElementById("list");

function listContent(array, parent) {
  let arrayMap = array.map((element) => {
    return `<li>${element}</li>`;
  }).join("");
  parent.innerHTML = arrayMap;
};

listContent(array, domPosition);

